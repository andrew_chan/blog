class ProductsController < ApplicationController

helper_method :sort_column, :sort_direction


  def new
    @product = Product.new
  end
  
  def create
    @product = Product.new(product_params.permit(:item_name,:item_description))
    if @product.save
      redirect_to @product
    else
      render 'new'
    end
  end
  
  def show
    @product = Product.find(params[:id])
    @rating = Rating.create(product_id: @product.id, user_id: current_user.id, score: 0)
  end
  
  def like
    @product = Product.find(params[:id])
    if Like.find_by_user_id_and_product_id(current_user.id,@product.id).nil?
      @like = Like.create(product_id: @product.id, user_id: current_user.id)
      flash[:notice] = "You successfully like the product ! "
    else
      flash[:error] = "You have already liked the product ! "
    end
    redirect_to @product
  end
  
  def dislike
    @product = Product.find(params[:id])
    @like = Like.find_by_user_id_and_product_id(current_user.id,@product.id)
    unless @like.nil? 
      @like.delete 
      flash[:notice] = "You successfully dislike the product ! "
    else
      flash[:error] = "You have never liked the product ! "
    end
    redirect_to @product
  end
  
  def index
    @products = Product.order(sort_column + " " + sort_direction)
    @products = @products.paginate(:page => params[:page], :per_page => 10)

  end
  
  def edit
    @product = Product.find(params[:id])
  end
  
  def update
    @product = Product.find(params[:id])
    if @product.update(params[:product].permit(:item_name, :item_description))
      redirect_to @product
    else
      render 'edit'
    end
  end
  
  def destroy
    @product = Product.find(params[:id])
    @product.destroy
    redirect_to products_path
  end
  
  def search
    @search = Product.search do
      keywords(params[:q])
      paginate(:page => params[:page], :per_page => 25)
    end
    @products = @search.results
  end
  
  private 
  def product_params
    params.require(:product).permit(:item_name,:item_description)
  end
  
  def sort_column
    Product.column_names.include?(params[:sort]) ? params[:sort] : "item_name"
  end
  
  def sort_direction
    %w[asc desc].include?(params[:direction]) ? params[:direction] : "asc"
  end
  
end
