class Product < ActiveRecord::Base
  searchable do
    text :item_name, :default_boost => 2
    text :item_description
  end

  has_many :ratings
  has_many :likes
  has_many :users, :through => :likes
  
  def average_rating
    ratings.sum(:score) / ratings.size
  end
end
