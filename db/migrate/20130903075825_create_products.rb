class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.integer :item_id
      t.integer :provider_id
      t.text :ref_id
      t.integer :brand_id
      t.text :source_url
      t.text :item_name
      t.text :item_description
      t.date :post_date
      t.timestamp :crawl_time
      t.binary :target_audience
      t.binary :season
      t.text :currency_code
      t.decimal :price
      t.decimal :tax_and_levy
      t.decimal :discount
      t.decimal :delivery_charges
      t.text :src_id
      t.binary :src_popular
      t.binary :in_stock
      t.text :temp_img_urls

      t.timestamps
    end
  end
end
